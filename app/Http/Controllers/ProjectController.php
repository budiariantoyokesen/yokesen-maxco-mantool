<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;
use Image;
use CRUDBooster;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Jenssegers\Agent\Agent;

class ProjectController extends Controller
{
  public function projectList()
  {
    $projects = DB::table('projects')->orderby('id', 'desc')->get();
    return view('pages.project-list', compact('projects'));
  }

  public function detail($id)
  {
    $project = DB::table('projects')->where('id', $id)->first();
    $activities = DB::table('activities')->where('activities.project_id', $id)->orderby('activities.id', 'desc')->get();
    $teams = DB::table('cms_users')->where('id_cms_privileges', '>', '1')->get();
    $todolist = DB::table('todolist')->where('project_id', $id)->orderby('sort', 'asc')->get();
    // dd('this');
    return view('pages.detail-project', compact('project', 'activities', 'teams', 'todolist', 'id'));
  }


  public function todoPost(Request $request)
  {
    $getTodo = DB::table('todolist')->where([
      ['project_id', '=', $request->project_id],
      // ['job_status','=', $request->todoStatus]
    ]);
    $deadline = date('Y-m-d H:i:s', strtotime($request->projectDeadline . " " . $request->projectDetailDeadline));

    $insert = DB::table('todolist')->insert([
      'project_id' => $request->project_id,
      'user_id' => $request->projectUser,
      'todo' => $request->projectToDo,
      'job_status' => $request->todoStatus,
      'sort' => $getTodo->count(),
      'deadline' => $deadline
    ]);
    return redirect()->back();
  }

  public function todoEdit(Request $request, $id)
  {

    if (empty($request->projectDeadline)) {
      $deadline = DB::table('todolist')->where('id', $id)->value('deadline');
    } else {
      $deadline = date('Y-m-d H:i:s', strtotime($request->projectDeadline . " " . $request->projectDetailDeadline));
    }
    //dd($request->all(),$deadline);
    $update = DB::table('todolist')->where('id', $id)->update([
      'job_status' => $request->todoStatus,
      'deadline' => $deadline
    ]);
    return redirect()->back();
  }

  public function updateSortListTodo(Request $request)
  {
    $originPosition = $request->originPosition;
    $updatePosition = $request->updatePosition;
    $id = $request->id;
    $project_id = $request->project_id;
    $user_id = $request->user_id;
    $getTodo = DB::table('todolist')->where([
      ['project_id', '=', $project_id],
      ['sort', '=', $updatePosition]
    ])->first();
    $step = "";
    // script dibawah ada karena ada data sort yg nilainya sama 
    // if($getTodo->count()==0 || $getTodo->count() > 1){
    //   $getTodo = DB::table('todolist')->where([
    //     ['project_id','=',$project_id]
    //   ])
    //   ->orderby('sort','asc')
    //   ->offset($updatePosition-1)
    //   ->limit(1)
    //   ->first();
    // }else{
    //   $getTodo = $getTodo->first();
    // }
    // geser ke bawah
    if ($updatePosition > $originPosition) {
      $step = "geser ke bawah";
      $update1 = DB::table('todolist')->where('id', $getTodo->id)->update(['sort' => $updatePosition - 1]);
    } else {
      $step = "geser ke atas";
      $update1 = DB::table('todolist')->where('id', $getTodo->id)->update(['sort' => $updatePosition + 1]);
    }
    $update2 = DB::table('todolist')->where('id', $id)->update(['sort' => $updatePosition]);
    dd($getTodo, $step);
  }

  public function edit(Request $request, $id)
  {

    if (!empty($request->bukti)) {
      $image = $request->bukti;
      list($width, $height, $type, $attr) = getimagesize($image);
      $time = time();
      $imgName  = str_replace(" ", "", $time . "-" . $image->getClientOriginalName());
      if ($width > $height) {
        $image_normal = Image::make($image)->widen(1200, function ($constraint) {
          $constraint->upsize();
        });
      } else {
        $image_normal = Image::make($image)->heighten(600, function ($constraint) {
          $constraint->upsize();
        });
      }

      $image_thumb = Image::make($image)->fit(200, 200);
      $image_normal = $image_normal->stream();
      $image_thumb = $image_thumb->stream();

      Storage::disk('local')->put('uploads/' . $imgName, $image_normal->__toString());
      Storage::disk('local')->put('uploads/thumbnails/' . $imgName, $image_thumb->__toString());
    }

    $insert = DB::table('activities')->insert([
      'description' => $request->description,
      'project_id' => $request->project_id,
      'image' => $imgName,
      'user_id' => $request->assign,
      'from_id' => CRUDBooster::myID(),
      'activity_id' => $request->activity_id,
      'created_at' => date('Y-m-d H:i:s'),

    ]);

    $recipient = [$request->assign];

    CRUDBooster::sendNotification($config = [
      'content' => 'Info baru dari ' . ucwords(CRUDBooster::me()->name) . ' project ' . $request->project_id,
      'to' => config('app.url') . '/project/detail/' . $request->project_id,
      'id_cms_users' => $recipient
    ]);
    return redirect()->back();
  }

  public function create()
  {
    $activities = DB::table('activities')->where('activities.project_id', $id)->orderby('activities.id', 'desc')->get();
    $teams = DB::table('cms_users')->where('id_cms_privileges', '>', '1')->get();
    $todolist = DB::table('todolist')->where('project_id', $id)->orderby('sort', 'asc')->get();
    return view('pages.create', compact('activities', 'teams', 'todoList'));
  }

  public function index()
  {
    $projects = DB::table('projects')->get();
    return view('pages.projectIndex', compact('projects'));
  }

  public function postCreate(Request $request)
  {
    $deadline = date('Y-m-d H:i:s', strtotime($request->projectDeadline . " " . $request->projectDetailDeadline));
    //dd($request->all());
    if (!empty($request->projectImage)) {
      $image = $request->projectImage;
      list($width, $height, $type, $attr) = getimagesize($image);
      $time = time();
      $imgName  = str_replace(" ", "", $time . "-" . $image->getClientOriginalName());
      if ($width > $height) {
        $image_normal = Image::make($image)->widen(1200, function ($constraint) {
          $constraint->upsize();
        });
      } else {
        $image_normal = Image::make($image)->heighten(600, function ($constraint) {
          $constraint->upsize();
        });
      }

      $image_thumb = Image::make($image)->fit(200, 200);
      $image_normal = $image_normal->stream();
      $image_thumb = $image_thumb->stream();

      Storage::disk('local')->put('uploads/' . $imgName, $image_normal->__toString());
      Storage::disk('local')->put('uploads/thumbnails/' . $imgName, $image_thumb->__toString());
    }

    $project = DB::table('projects')->insert([
      'nama_project' => $request->projectName,
      'project_desc' => $request->projectDescription,
      'project_img' => $imgName,
      'user_id' => CRUDBooster::myID(),
      'status' => 'active',
      'deadline_at' => $deadline
    ]);

    return redirect()->route('projectIndex');
  }

  public function edit_post_calendar($id)
  {
    $post = DB::table('post')->where('id', $id)->first();
    $comments = DB::table('comments')->where('post_id', $id)->get();
    $imgcmts = DB::table('comments')->where('post_id', $id)->where('image', '!=', 'tidak')->get();
    $agent = new Agent();
    $datetime = date('Y-m-d H:i:s');
    if (strtotime($post->postTimeFbVid) < strtotime($datetime)) {
      $expired = 'yes';
    } else {
      $expired = 'no';
    }

    if(CRUDBooster::myPrivilegeID() == '7'){
      $expired = 'no';
    }

    if (!empty($post->postStatusApproval)) {
      $expired = 'yes';
    }
    return view('pages.edit', compact('post', 'comments', 'agent', 'expired', 'imgcmts'));
  }

  public function approval_producer($id)
  {
    $agent = new Agent();
    $user = CRUDBooster::myID();
    $datetime = date('Y-m-d H:i:s');

    $save = [
      'agent' => $agent,
      'user' => $user,
      'time' => $datetime,
      'ipaddress' => \Request::ip()
    ];

    $simpan = serialize($save);

    $update = DB::table('post')->where('id', $id)->update([
      'postStatusProducer' => $simpan
    ]);

    $user = DB::table('cms_users')->where('id_cms_privileges', '!=', '4')->get();
    foreach ($user as $isi) {
      $recipient[] = $isi->id;
    }
    $post = DB::table('post')->where('id', $id)->first();
    CRUDBooster::sendNotification($config = [
      'content' => CRUDBooster::myName() . ' Approval [P] ' . $post->postName,
      'to' => env('APP_URL') . '/calendar/edit/' . $id,
      'id_cms_users' => $recipient
    ]);

    return redirect()->back();
  }

  public function approval_editor($id)
  {
    $agent = new Agent();
    $user = CRUDBooster::myID();
    $datetime = date('Y-m-d H:i:s');

    $save = [
      'agent' => $agent,
      'user' => $user,
      'time' => $datetime,
      'ipaddress' => \Request::ip()
    ];

    $simpan = serialize($save);

    $update = DB::table('post')->where('id', $id)->update([
      'postStatusEditor' => $simpan
    ]);

    $user = DB::table('cms_users')->where('id_cms_privileges', '!=', '4')->get();
    foreach ($user as $isi) {
      $recipient[] = $isi->id;
    }
    $post = DB::table('post')->where('id', $id)->first();
    CRUDBooster::sendNotification($config = [
      'content' => CRUDBooster::myName() . ' Approval [E] ' . $post->postName,
      'to' => env('APP_URL') . '/calendar/edit/' . $id,
      'id_cms_users' => $recipient
    ]);

    return redirect()->back();
  }

  public function approval_client($id)
  {
    $agent = new Agent();
    $user = CRUDBooster::myID();
    $datetime = date('Y-m-d H:i:s');

    $save = [
      'agent' => $agent,
      'user' => $user,
      'time' => $datetime,
      'ipaddress' => \Request::ip()
    ];

    $simpan = serialize($save);

    $update = DB::table('post')->where('id', $id)->update([
      'postStatusApproval' => $simpan
    ]);

    $user = DB::table('cms_users')->where('id_cms_privileges', '!=', '4')->get();
    foreach ($user as $isi) {
      $recipient[] = $isi->id;
    }
    $post = DB::table('post')->where('id', $id)->first();
    CRUDBooster::sendNotification($config = [
      'content' => CRUDBooster::myName() . ' Approval [C] ' . $post->postName,
      'to' => env('APP_URL') . '/calendar/edit/' . $id,
      'id_cms_users' => $recipient
    ]);

    return redirect()->back();
  }

  public function post_calendar_comment(Request $request, $id)
  {
    if ($request->image) {
      $imaging = $request->image - 1;
    } else {
      $imaging = "tidak";
    }

    $insert = DB::table('comments')->insert([
      'post_id' => $id,
      'user_id' => CRUDBooster::myID(),
      'comment' => $request->comment,
      'image' => $imaging
    ]);

    $user = DB::table('cms_users')->where('id_cms_privileges', '!=', '4')->get();
    foreach ($user as $isi) {
      $recipient[] = $isi->id;
    }

    $post = DB::table('post')->where('id', $id)->first();
    CRUDBooster::sendNotification($config = [
      'content' => 'New Comment utk post ' . $post->postName,
      'to' => env('APP_URL') . '/calendar/edit/' . $id,
      'id_cms_users' => $recipient
    ]);

    return redirect()->back();
  }

  public function imageDelete($postID, $imageID)
  {
    $post = DB::table('post')->where('id', $postID)->first();
    $images = unserialize($post->postImages);
    // dd($images);
    // array_splice($images, $imageID, 1);
    $images[$imageID]['isdeleted']=1;
    $image_save = serialize($images);
    $update = DB::table('post')->where('id', $postID)->update([
      'postImages' => $image_save
    ]);
    return redirect()->back();
  }

  public function displayAdd(Request $request, $postID)
  {
    $post = DB::table('post')->where('id', $postID)->first();
    $images = unserialize($post->postImages);
    if (!empty($request->photos)) {
      foreach ($request->photos as $photo) {
        $image = $photo;
        list($width, $height, $type, $attr) = getimagesize($image);
        $time = time();
        $imgName  = str_replace(" ", "", $time . "-" . $image->getClientOriginalName());
        if ($width > $height) {
          $image_normal = Image::make($image)->widen(1200, function ($constraint) {
            $constraint->upsize();
          });
        } else {
          $image_normal = Image::make($image)->heighten(600, function ($constraint) {
            $constraint->upsize();
          });
        }
        // dd($imgName);
        $arrayName[] = [
          "url" => $imgName,
          "isdeleted"=>0
        ];

        $image_thumb = Image::make($image)->fit(200, 200);
        $image_normal = $image_normal->stream();
        $image_thumb = $image_thumb->stream();

        Storage::disk('local')->put('uploads/' . $imgName, $image_normal->__toString());
        Storage::disk('local')->put('uploads/thumbnails/' . $imgName, $image_thumb->__toString());
      }
    }
    if ($images!=false&&count($images) > 0) {
      foreach ($arrayName as $baru) {
        array_push($images, $baru);
      }
    } else {
      $images = $arrayName;
    }

    $saveimg = serialize($images);
    $post = DB::table('post')->where('id', $postID)->update([
      'postImages' => $saveimg
    ]);

    return redirect()->back();
  }

  public function videoAdd(Request $request, $id)
  {
    $post = DB::table('post')->where('id', $id)->update([
      'postVideo' => $request->postVideo
    ]);
    return redirect()->back();
  }

  public function delete_post_calendar($id)
  {
    $post = DB::table('post')->where('id', $id)->update([
      'status' => 'inactive'
    ]);
    return redirect()->back();
  }

  public function comment_gambar(Request $request, $id)
  {

    dd($request->all(), $id);
    return redirect()->back();
  }

  public function postToStatus($id, $type, $status)
  {
    $post = DB::table('post')->where('id', $id)->update([
      $type => $status
    ]);
    return redirect()->back();
  }

  public function editing_post_calendar(Request $request, $id)
  {
    $post = DB::table('post')->where('id', $id)->update([
      'postName' => $request->postName,
      'postCaption' => $request->postCaption,
      'postFbGroup' => $request->postFbGroup,
      'postTimeYoutube' => $request->postTimeYoutube,
      'postTimeFbVid' => $request->postTimeFbVid,
      'postTimeIgTv' => $request->postTimeIgTv,
      'postTimeIgFeed' => $request->postTimeIgFeed,
      'postTimeStories' => $request->postTimeStories,
      'postTimeFbGroup' => $request->postTimeFbGroup,
      'postAdsYoutubeType' => $request->postAdsYoutubeType,
      'postAdsYoutubeCost' => $request->postAdsYoutubeCost,
      'postAdsYoutubeKey' => $request->postAdsYoutubeKey,
      'postAdsYoutubeDays' => $request->postAdsYoutubeDays,
      'postAdsFbType' => $request->postAdsFbType,
      'postAdsFbCost' => $request->postAdsFbCost,
      'postAdsFbKey' => $request->postAdsFbKey,
      'postAdsFbDays' => $request->postAdsFbDays,
      'postAdsIgType' => $request->postAdsIgType,
      'postAdsIgCost' => $request->postAdsIgCost,
      'postAdsIgKey' => $request->postAdsIgKey,
      'postAdsIgDays' => $request->postAdsIgDays,
    ]);
    return redirect()->back();
  }

  public function done($id)
  {
    $todo = DB::table('todolist')->where('id', $id)->first();
    $last = DB::table('todolist')->where('project_id', $todo->project_id)->orderby('sort', 'desc')->first();
    $max = $last->sort + 1;
    $update = DB::table('todolist')->where('id', $id)->update([
      'job_status' => 'done',
      'sort' => $max
    ]);

    $pic = DB::table('cms_users')->where('id', $todo->user_id)->first();

    $insert = DB::table('activities')->insert([
      'description' => $pic->name . ' telah menyelesaikan ' . $todo->todo . ' pada ' . date('d-F-Y H:i') . ' dari deadline ' . date('d-F-Y H:i', strtotime($todo->deadline)),
      'project_id' => $todo->project_id,
      'image' => '',
      'user_id' => $pic->id,
      'from_id' => CRUDBooster::myID(),
      'activity_id' => '0',
      'created_at' => date('Y-m-d H:i:s'),
      'deadline_at' => $todo->deadline
    ]);
    return redirect()->back();
  }

  public function calendar()
  {
    $datenow = date('Y-m-d H:i:s');
    $expire = date('Y-m-d H:i:s', strtotime($datenow . " -28 days"));
    if (CRUDBooster::myPrivilegeID() == '7') { //producer edo
      $post = DB::table('post')->where('status', 'active')->orderby('postTimeFbVid', 'desc')->where('postTimeFbVid', '>', $expire)->get();
    } elseif (CRUDBooster::myPrivilegeID() == '6') { //editor imel
      $post = DB::table('post')->where('status', 'active')->orderby('postTimeFbVid', 'desc')->where('postTimeFbVid', '>', $expire)->whereNotNull('postStatusProducer')->get();
    } elseif (CRUDBooster::myPrivilegeID() == '5') { //client sinde
      $post = DB::table('post')->where('status', 'active')->orderby('postTimeFbVid', 'desc')->where('postTimeFbVid', '>', $expire)->whereNotNull('postStatusEditor')->get();
    } else {
      $post = DB::table('post')->where('status', 'active')->orderby('postTimeFbVid', 'desc')->where('postTimeFbVid', '>', $expire)->get();
    }


    $postCalendar = DB::table('post')->where('status', 'active')->get();

    return view('pages.calendar', compact('post', 'expire', 'datenow', 'postCalendar'));
  }

  public function new_post_calendar(Request $request)
  {
    if (!empty($request->photos)) {
      foreach ($request->photos as $image) {
        list($width, $height, $type, $attr) = getimagesize($image);
        $time = time();
        $imgName  = str_replace(" ", "", $time . "-" . $image->getClientOriginalName());
        if ($width > $height) {
          $image_normal = Image::make($image)->widen(1200, function ($constraint) {
            $constraint->upsize();
          });
        } else {
          $image_normal = Image::make($image)->heighten(600, function ($constraint) {
            $constraint->upsize();
          });
        }

        $arrayName[]= [
          "url" => $imgName,
          "isdeleted"=>0
        ];

        $image_thumb = Image::make($image)->fit(200, 200);
        $image_normal = $image_normal->stream();
        $image_thumb = $image_thumb->stream();

        Storage::disk('local')->put('uploads/' . $imgName, $image_normal->__toString());
        Storage::disk('local')->put('uploads/thumbnails/' . $imgName, $image_thumb->__toString());
      }
    }

    $imagesSerialize = serialize($arrayName);

    if ($request->postTimeYoutube == '') {
      $postToYoutube = '0';
    }

    if ($request->postTimeFbVid == '') {
      $postToFbVid = '0';
    }

    if ($request->postTimeIgTv == '') {
      $postToIgTv = '0';
    }

    if ($request->postTimeIgFeed == '') {
      $postToIgFeed = '0';
    }

    if ($request->postTimeStories == '') {
      $postToStories = '0';
    }

    if ($request->postTimeFbGroup == '') {
      $postToFbGroup = '0';
    }

    $insert = DB::table('post')->insert([
      'postName' => $request->postName,
      'postCaption' => $request->postCaption,
      'postImages' => $imagesSerialize,
      'postTimeYoutube' => $request->postTimeYoutube,
      'postTimeFbVid' => $request->postTimeFbVid,
      'postTimeIgTv' => $request->postTimeIgTv,
      'postTimeIgFeed' => $request->postTimeIgFeed,
      'postTimeStories' => $request->postTimeStories,
      'postTimeFbGroup' => $request->postTimeFbGroup,
      'postToYoutube' => '1',
      'postToFbVid' => '1',
      'postToIgTv' => '1',
      'postToIgFeed' => '1',
      'postToStories' => '1',
      'postToFbGroup' => '1',
      'created_at' => date('Y-m-d H:i:s')
    ]);
    return redirect()->back();
  }

  public function approval_satuan($id, $approval)
  {
    $agent = new Agent();
    $user = CRUDBooster::myID();
    $datetime = date('Y-m-d H:i:s');

    $save = [
      'agent' => $agent,
      'user' => $user,
      'time' => $datetime,
      'ipaddress' => \Request::ip()
    ];

    $simpan = serialize($save);

    $update = DB::table('post')->where('id', $id)->update([
      $approval => $simpan
    ]);

    $user = DB::table('cms_users')->where('id_cms_privileges', '!=', '4')->get();
    foreach ($user as $isi) {
      $recipient[] = $isi->id;
    }

    $post = DB::table('post')->where('id', $id)->first();
    CRUDBooster::sendNotification($config = [
      'content' => CRUDBooster::myName() . ' ' . $approval . ' ' . $post->postName,
      'to' => env('APP_URL') . '/calendar/edit/' . $id,
      'id_cms_users' => $recipient
    ]);

    return redirect()->back();
  }
}
