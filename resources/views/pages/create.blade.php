@extends("crudbooster::admin_template")

@section('csslineup')
  <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Bootstrap time Picker -->
 <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.css">
@endsection

@section("content")
  <div class="row">
    <div class="col-md-6">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          Create New Project
        </div>
        <!-- /.box-body -->
        <div class="box-body">
          <div class="col-md-12">
            <form class="form-horizontal" action="{{route('ProjectCreator')}}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="projectName">Project Name:</label>
                <input type="text" class="form-control" name="projectName" placeholder="Maxlength 140" maxlength="140" required/>
              </div>
              <div class="form-group">
                <label for="projectDescription">Project Description:</label>
                <textarea name="projectDescription" rows="2" class="form-control" required></textarea>
              </div>
              <div class="form-group">
                <label for="projectImage">Project Image:</label>
                <input type="file" class="form-control" name="projectImage" required/>
              </div>
              <div class="form-group">
                <label for="projectDeadline">Project Deadline:</label>
                <input type="date" class="form-control" name="projectDeadline" required/>
              </div>
              <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Detail Deadline:</label>
                  <div class="input-group">
                    <input type="text" name="projectDetailDeadline" class="form-control timepicker">

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
              
              <div class="form-group">
                <input type="submit" class="form-control" value="SUBMIT"/>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
    <div class="col-md-6">

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
@endsection

@section('jslineup')
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/knob/jquery.knob.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>

  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/dist/js/pages/dashboard.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.js"  type="text/javascript"></script>

@endsection

@section('jsonpage')

<script type="text/javascript">

//Timepicker
  $('.timepicker').timepicker({
    showInputs: true
  })


</script>

@endsection
