@extends("crudbooster::admin_template")

@section('csslineup')
  <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/fullcalendar/fullcalendar.min.css">

@endsection

@section("content")
  <div class="row">

    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <!-- THE CALENDAR -->
          <div id="calendar"></div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3">
      @if(CRUDBooster::myPrivilegeID()!=4)
      <div class="row">
        <div class="col-md-12">

          <div class="box box-danger collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Post</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="{{route('ProjectPostCalendar')}}" method="post" name="addPostForm" id="addPostForm" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="postName">Post Name:</label>
                  <input type="text" class="form-control" name="postName" placeholder="Maxlength 12" maxlength="12" required/>
                </div>
                <div class="form-group">
                  <label for="photo">Post Images (Drag here):</label>
                  <input type="file" class="form-control" name="photos[]" multiple required/>
                </div>
                <div class="form-group">
                  <label for="postCaption">Post Caption</label>
                  <textarea name="postCaption" class="form-control" form="addPostForm" rows="4" placeholder="Caption here.." ></textarea>
                </div>
                <div class="input-group">
                  <label for="postTimeYoutube">Youtube Post Time</label>
                  <input type='text' title="Youtube Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeYoutube" id="postTimeYoutube" value=''/>
                </div>
                <div class="input-group">
                  <label for="postTimeFbVid">Facebook Post Time</label>
                  <input type='text' title="Facebook Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeFbVid" id="postTimeFbVid" value=''/>
                </div>
                <div class="input-group">
                  <label for="postTimeIgTv">Instagram TV Post Time</label>
                  <input type='text' title="Instagram TV Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeIgTv" id="postTimeIgTv" value=''/>
                </div>
                <div class="input-group">
                  <label for="postTimeIgFeed">Instagram Feed Time</label>
                  <input type='text' title="Instagram Feed Time" readonly required class='form-control notfocus datetimepicker' name="postTimeIgFeed" id="postTimeIgFeed" value=''/>
                </div>
                <div class="input-group">
                  <label for="postTimeStories">All Stories Post Time</label>
                  <input type='text' title="All Stories Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeStories" id="postTimeStories" value=''/>
                </div>
                <div class="input-group">
                  <label for="postTimeFbGroup">Fb Group Post Time</label>
                  <input type='text' title="Fb Group Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeFbGroup" id="postTimeFbGroup" value=''/>
                </div>
                <hr>
                <div class="form-group">
                  <input type="submit" name="submit" value="SUBMIT" class="btn btn-block btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      @endif
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Post Plan</h3>
            </div>
            <div class="box-body">
              @foreach($post as $n => $isi)
                @if($isi->postTimeFbVid > $expire)
                <?php
                  $isiImg = [];
                  if(!empty($isi->postImages)){
                    $postImages=unserialize($isi->postImages);
                    foreach($postImages as $key=>$image){
                      if(isset($image['isdeleted'])&&$image['isdeleted']==0){
                        $isiImg[]=$image['url'];
                      }
                    }
                  }
                  
                ?>
                    <div class="col-sm-4" style="padding:2px!important;">
                      <img src="{{url('/')}}/uploads/thumbnails/{{$isiImg[0]}}" width="100%" data-toggle="modal" data-target="#modal-{{$isi->id}}"/>

                        @if(empty($isi->postStatusProducer))
                          <i class="fa fa-clock-o text-danger"></i>
                        @else
                          <i class="fa fa-check-square-o text-success"></i>
                        @endif

                        @if(empty($isi->postStatusEditor))
                          <i class="fa fa-clock-o text-danger"></i>
                        @else
                          <i class="fa fa-check-square-o text-success"></i>
                        @endif

                        @if(empty($isi->postStatusApproval))
                          <i class="fa fa-clock-o text-danger"></i>
                        @else
                          <i class="fa fa-check-square-o text-success"></i>
                        @endif

                    </div>
                @endif
              @endforeach

            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  @foreach ($post as $modal)
    @if($modal->postTimeFbVid > $expire)
    <?php
      $modalImages =[];
      if(!empty($modal->postImages)){
        $postImages= unserialize($modal->postImages);
        foreach($postImages as $key=>$image){
          if(isset($image['isdeleted'])&&$image['isdeleted']==0){
            $modalImages[]=$image['url'];
          }
        }
      }
    ?>
    <div class="modal fade" id="modal-{{$modal->id}}">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{$modal->postName}}</h4>
          </div>
          <div class="modal-body">
            <div id="carousel-example-generic-{{$modal->id}}" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                @if($modalImages)
                  @foreach ($modalImages as $n => $nex)
                    <li data-target="#carousel-example-generic-{{$modal->id}}" data-slide-to="{{$n}}" class="{{$n == 0 ? 'active' : ''}}"></li>
                  @endforeach
                @endif
              </ol>
              <div class="carousel-inner">
                @if($modalImages)
                  @foreach($modalImages as $x => $gbr)
                    <div class="item {{$x == 0 ? 'active' : ''}}">
                      <img src="{{url('/')}}/uploads/{{$gbr}}" width="100%" />
                    </div>
                  @endforeach
                @endif
              </div>
              <a class="left carousel-control" href="#carousel-example-generic-{{$modal->id}}" data-slide="prev">
                <span class="fa fa-angle-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic-{{$modal->id}}" data-slide="next">
                <span class="fa fa-angle-right"></span>
              </a>
            </div>
            <p><b>Bayer</b> - {!! nl2br($modal->postCaption) !!}</p>
            @if(!empty($modal->postVideo))
              <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{$modal->postVideo}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            @endif
            <p>Facebook Group Target Communities : <b>{{$modal->postFbGroup}}</b></p>
            <hr>
            <table class="table table-bordered">
              <tr>
                <th style="width: 10px">#</th>
                <th>Platform</th>
                <th>Schedule</th>
                <th>Approval</th>
              </tr>
              <tr>
                <td>1.</td>
                <td>Youtube Post Time</td>
                @if($modal->postToYoutube == '1')
                  <td>{{$modal->postTimeYoutube != '' ? date('d-F-Y H:i',strtotime($modal->postTimeYoutube)) : date('1990-07-20 19:00')}}</td>
                  @if(!empty($modal->postApproveYoutube))
                    <td><b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b></td>
                  @else
                    <td>--</td>
                  @endif
                @else
                  <td>Tidak Tayang</td>
                  <td>--</td>
                @endif

              </tr>
              <tr>
                <td>2.</td>
                <td>Facebook Post Time</td>
                @if($modal->postToFbVid == '1')
                  <td>{{$modal->postTimeFbVid != '' ? date('d-F-Y H:i',strtotime($modal->postTimeFbVid)) : date('1990-07-20 19:00')}}</td>
                  @if(!empty($modal->postApproveFbVid))
                    <td><b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b></td>
                  @else
                    <td>--</td>
                  @endif
                @else
                  <td>Tidak Tayang</td>
                  <td>--</td>
                @endif

              </tr>
              <tr>
                <td>3.</td>
                <td>Instagram TV Post Time</td>
                @if($modal->postToIgTv == '1')
                  <td>{{$modal->postTimeIgTv != '' ? date('d-F-Y H:i',strtotime($modal->postTimeIgTv)) : date('1990-07-20 19:00')}}</td>
                  @if(!empty($modal->postApproveIgTv))
                    <td><b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b></td>
                  @else
                    <td>--</td>
                  @endif
                @else
                  <td>Tidak Tayang</td>
                  <td>--</td>
                @endif

              </tr>
              <tr>
                <td>4.</td>
                <td>Instagram Feed Time</td>
                @if($modal->postToIgFeed == '1')
                  <td>{{$modal->postTimeIgFeed != '' ? date('d-F-Y H:i',strtotime($modal->postTimeIgFeed)) : date('1990-07-20 19:00')}}</td>
                  @if(!empty($modal->postApproveIgFeed))
                    <td><b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b></td>
                  @else
                    <td>--</td>
                  @endif
                @else
                  <td>Tidak Tayang</td>
                  <td>--</td>
                @endif

              </tr>
              <tr>
                <td>5.</td>
                <td>All Stories Post Time</td>
                @if($modal->postToStories == '1')
                  <td>{{$modal->postTimeStories != '' ? date('d-F-Y H:i',strtotime($modal->postTimeStories)) : date('1990-07-20 19:00')}}</td>
                  @if(!empty($modal->postApproveFbGroup))
                    <td><b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b></td>
                  @else
                    <td>--</td>
                  @endif
                @else
                  <td>Tidak Tayang</td>
                  <td>--</td>
                @endif

              </tr>
              <tr>
                <td>6.</td>
                <td>Fb Group Post Time</td>
                @if($modal->postToFbGroup == '1')
                  <td>{{$modal->postTimeFbGroup != '' ? date('d-F-Y H:i',strtotime($modal->postTimeFbGroup)) : date('1990-07-20 19:00')}}</td>
                  @if(!empty($modal->postApproveFbGroup))
                    <td><b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b></td>
                  @else
                    <td>--</td>
                  @endif
                @else
                  <td>Tidak Tayang</td>
                  <td>--</td>
                @endif

              </tr>
            </table>
            @if(CRUDBooster::myPrivilegeID()!=4)
            <table class="table table-bordered">
              <tr>
                <th style="width: 10px">#</th>
                <th>Platform</th>
                <th>Ads type</th>
                <th>KPI</th>
                <th>Cost</th>
                <th>Days</th>
              </tr>
              <tr>
                <td>1.</td>
                <td>Youtube</td>
                <td>{{$modal->postAdsYoutubeType}}</td>
                <td>{{$modal->postAdsYoutubeKey}}</td>
                <td>{{number_format($modal->postAdsYoutubeCost,0,".",".")}}</td>
                <td>{{$modal->postAdsYoutubeDays}}</td>
              </tr>
              <tr>
                <td>2.</td>
                <td>Facebook</td>
                <td>{{$modal->postAdsFbType}}</td>
                <td>{{$modal->postAdsFbKey}}</td>
                <td>{{number_format($modal->postAdsFbCost,0,".",".")}}</td>
                <td>{{$modal->postAdsFbDays}}</td>
              </tr>
              <tr>
                <td>3.</td>
                <td>Instagram</td>
                <td>{{$modal->postAdsIgType}}</td>
                <td>{{$modal->postAdsIgKey}}</td>
                <td>{{number_format($modal->postAdsIgCost,0,".",".")}}</td>
                <td>{{$modal->postAdsIgDays}}</td>
              </tr>
            </table>
          @endif
          </div>
          @if(CRUDBooster::myPrivilegeID()!=4)
          <div class="modal-footer">
            <a href="{{route('ProjectEditPostCalendar',$modal->id)}}" type="button" class="btn btn-primary">Edit Post</a>
          </div>
          @endif
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  @endif
  @endforeach
@endsection

@section('jslineup')
<script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
@endsection

@section('jsonpage')

<script type="text/javascript">
$(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    init_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
      },
      //Random default events
      events    : [
        @foreach ($postCalendar as $event)
          @if($event->postToYoutube == '1')
            {
              title          : '{{$event->postName}} YT',
              start          : '{{$event->postTimeYoutube != '' ? $event->postTimeYoutube : date('1990-07-20 19:00')}}',
              backgroundColor: '#FF2006',
              borderColor    : '#FF2006',
              @if(CRUDBooster::myPrivilegeID()!=4)
              url            : '{{route('ProjectEditPostCalendar',$event->id)}}',
              @endif
            },
          @endif
          @if($event->postToFbVid == '1')
            {
              title          : '{{$event->postName}} FBPost',
              start          : '{{$event->postTimeFbVid != '' ? $event->postTimeFbVid : date('1990-07-20 19:00')}}',
              backgroundColor: '#337DFF',
              borderColor    : '#337DFF',
              @if(CRUDBooster::myPrivilegeID()!=4)
              url            : '{{route('ProjectEditPostCalendar',$event->id)}}',
              @endif
            },
          @endif
          @if($event->postToIgTv == '1')
            {
              title          : '{{$event->postName}} IGTV',
              start          : '{{$event->postTimeIgTv != '' ? $event->postTimeIgTv : date('1990-07-20 19:00')}}',
              backgroundColor: '#f39c12',
              borderColor    : '#f39c12',
              @if(CRUDBooster::myPrivilegeID()!=4)
              url            : '{{route('ProjectEditPostCalendar',$event->id)}}',
              @endif
            },
          @endif
          @if($event->postToIgFeed == '1')
            {
              title          : '{{$event->postName}} IGFeed',
              start          : '{{$event->postTimeIgFeed != '' ? $event->postTimeIgFeed : date('1990-07-20 19:00')}}',
              backgroundColor: '#FF33CE',
              borderColor    : '#FF33CE',
              @if(CRUDBooster::myPrivilegeID()!=4)
              url            : '{{route('ProjectEditPostCalendar',$event->id)}}',
              @endif
            },
          @endif
          @if($event->postToStories == '1')
            {
              title          : '{{$event->postName}} Stories',
              start          : '{{$event->postTimeStories != '' ? $event->postTimeStories : date('1990-07-20 19:00')}}',
              backgroundColor: '#00a65a',
              borderColor    : '#00a65a',
              @if(CRUDBooster::myPrivilegeID()!=4)
              url            : '{{route('ProjectEditPostCalendar',$event->id)}}',
              @endif
            },
          @endif
          @if($event->postToFbGroup == '1')
            {
              title          : '{{$event->postName}} FBGroup',
              start          : '{{$event->postTimeFbGroup != '' ? $event->postTimeFbGroup : date('1990-07-20 19:00')}}',
              backgroundColor: '#01399A',
              borderColor    : '#01399A',
              @if(CRUDBooster::myPrivilegeID()!=4)
              url            : '{{route('ProjectEditPostCalendar',$event->id)}}',
              @endif
            },
          @endif
        @endforeach
      ],
      eventClick: function(info) {
        info.jsEvent.preventDefault(); // don't let the browser navigate

        if (info.event.url) {
          window.open(info.event.url);
        }
      },
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove()
        }

      }
    })



    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      init_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })
</script>

@endsection
