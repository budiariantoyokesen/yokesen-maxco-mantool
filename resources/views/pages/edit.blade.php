@extends("crudbooster::admin_template")

@section("content")
  <div class="row">
    <?php
      $displays=[];
      $displayAll=[];
      $postImages = unserialize($post->postImages);
      if($postImages!=false&&count($postImages)>0){
        foreach($postImages as $key=>$image){
          if($image['isdeleted']==0){
            $displays[$key]=$image['url'];
          }
          $displayAll[$key]=$image['url'];
        }
      }
      
    ?>
    <div class="col-md-8">
      <div class="box box-warning">
        <div class="box-header">
          <h3 class="box-title">Edit Post</h3>
        </div>
        <div class="box-body">
          <form action="{{route('ProjectEditingPostCalendar',$post->id)}}" method="post" name="addPostForm" id="addPostForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="postName">Post Name:</label>
              <input type="text" class="form-control" name="postName" placeholder="Maxlength 12" maxlength="12" value="{{$post->postName}}" required {{$expired == "yes" ? "readonly" : ""}}/>
            </div>
            <div class="form-group">
              <label for="postCaption">Post Caption</label>
              <textarea name="postCaption" class="form-control" form="addPostForm" rows="12"  {{$expired == "yes" ? "readonly" : ""}}>{{$post->postCaption}}</textarea>
            </div>
            <div class="form-group">
              <label for="postCaption">Facebook Group (Community Target)</label>
              <textarea name="postFbGroup" class="form-control" form="addPostForm" rows="3"  placeholder="pisahkan dengan koma" {{$expired == "yes" ? "readonly" : ""}}>{{$post->postFbGroup}}</textarea>
            </div>
            <hr>
            @if(CRUDBooster::myPrivilegeID() == '5')
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Platform</th>
                    <th>Schedule</th>
                    <th>Status Tayang</th>
                    <th>Approval Tayang</th>
                  </tr>
                  <tr>
                    <td>1.</td>
                    <td>Youtube Post Time</td>
                    <td>{{$post->postTimeYoutube != '' ? date('d-F-Y H:i',strtotime($post->postTimeYoutube)) : '-'}}</td>
                    @if($post->postToYoutube == '1')
                      <td> <a href="{{route('editPostTo',[$post->id,'postToYoutube','0'])}}" class="btn btn-block btn-success">On</a> </td>
                      @if(empty($post->postApproveYoutube))
                        <td> <a href="{{route('approvalSatuan',[$post->id,'postApproveYoutube'])}}" class="btn btn-block btn-danger">Approvall</a> </td>
                      @else
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                      @endif
                    @else
                      <td> <a href="{{route('editPostTo',[$post->id,'postToYoutube','1'])}}" class="btn btn-block btn-danger">Off</a> </td>
                      <td> off </td>
                    @endif


                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>Facebook Video Post Time</td>
                    <td>{{$post->postTimeFbVid != '' ? date('d-F-Y H:i',strtotime($post->postTimeFbVid)) : '-'}}</td>
                    @if($post->postToFbVid == '1')
                      <td> <a href="{{route('editPostTo',[$post->id,'postToFbVid','0'])}}" class="btn btn-block btn-success">On</a> </td>
                      @if(empty($post->postApproveFbVid))
                        <td> <a href="{{route('approvalSatuan',[$post->id,'postApproveFbVid'])}}" class="btn btn-block btn-danger">Approvall</a> </td>
                      @else
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                      @endif
                    @else
                      <td> <a href="{{route('editPostTo',[$post->id,'postToFbVid','1'])}}" class="btn btn-block btn-danger">Off</a> </td>
                      <td> off </td>
                    @endif


                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>Instagram TV Post Time</td>
                    <td>{{$post->postTimeIgTv != '' ? date('d-F-Y H:i',strtotime($post->postTimeIgTv)) : '-'}}</td>
                    @if($post->postToIgTv == '1')
                      <td> <a href="{{route('editPostTo',[$post->id,'postToIgTv','0'])}}" class="btn btn-block btn-success">On</a> </td>
                      @if(empty($post->postApproveIgTv))
                        <td> <a href="{{route('approvalSatuan',[$post->id,'postApproveIgTv'])}}" class="btn btn-block btn-danger">Approvall</a> </td>
                      @else
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                      @endif
                    @else
                      <td> <a href="{{route('editPostTo',[$post->id,'postToIgTv','1'])}}" class="btn btn-block btn-danger">Off</a> </td>
                      <td> off </td>
                    @endif

                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>Instagram Feed Time</td>
                    <td>{{$post->postTimeIgFeed != '' ? date('d-F-Y H:i',strtotime($post->postTimeIgFeed)) : '-'}}</td>
                    @if($post->postToIgFeed == '1')
                      <td> <a href="{{route('editPostTo',[$post->id,'postToIgFeed','0'])}}" class="btn btn-block btn-success">On</a> </td>
                      @if(empty($post->postApproveIgFeed))
                        <td> <a href="{{route('approvalSatuan',[$post->id,'postApproveIgFeed'])}}" class="btn btn-block btn-danger">Approvall</a> </td>
                      @else
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                      @endif
                    @else
                      <td> <a href="{{route('editPostTo',[$post->id,'postToIgFeed','1'])}}" class="btn btn-block btn-danger">Off</a> </td>
                      <td> off </td>
                    @endif

                  </tr>
                  <tr>
                    <td>5.</td>
                    <td>All Stories Post Time</td>
                    <td>{{$post->postTimeStories != '' ? date('d-F-Y H:i',strtotime($post->postTimeStories)) : '-'}}</td>
                    @if($post->postToStories == '1')
                      <td> <a href="{{route('editPostTo',[$post->id,'postToStories','0'])}}" class="btn btn-block btn-success">On</a> </td>
                      @if(empty($post->postApproveStories))
                        <td> <a href="{{route('approvalSatuan',[$post->id,'postApproveStories'])}}" class="btn btn-block btn-danger">Approvall</a> </td>
                      @else
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                      @endif
                    @else
                      <td> <a href="{{route('editPostTo',[$post->id,'postToStories','1'])}}" class="btn btn-block btn-danger">Off</a> </td>
                      <td> off </td>
                    @endif

                  </tr>
                  <tr>
                    <td>6.</td>
                    <td>Fb Group Post Time</td>
                    <td>{{$post->postTimeFbGroup != '' ? date('d-F-Y H:i',strtotime($post->postTimeFbGroup)) : '-'}}</td>
                    @if($post->postToFbGroup == '1')
                      <td> <a href="{{route('editPostTo',[$post->id,'postToFbGroup','0'])}}" class="btn btn-block btn-success">On</a> </td>
                      @if(empty($post->postApproveFbGroup))
                        <td> <a href="{{route('approvalSatuan',[$post->id,'postApproveFbGroup'])}}" class="btn btn-block btn-danger">Approvall</a> </td>
                      @else
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                      @endif
                    @else
                      <td> <a href="{{route('editPostTo',[$post->id,'postToFbGroup','1'])}}" class="btn btn-block btn-danger">Off</a> </td>
                      <td> off </td>
                    @endif

                  </tr>
                </table>
              </div>
            </div>

            @else
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered">
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Platform</th>
                      <th>Schedule</th>
                      <th>Status Tayang</th>
                      <th>Approval Tayang</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td>Youtube Post Time</td>
                      <td><input type='text' title="Youtube Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeYoutube" id="postTimeYoutube" value='{{$post->postTimeYoutube != '' ? $post->postTimeYoutube : date('1990-07-20 19:00')}}'/></td>
                      @if($post->postToYoutube == '1')
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Tayang</b> </td>
                        @if(empty($post->postApproveYoutube))
                          <td> -- </td>
                        @else
                          <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                        @endif
                      @else
                        <td> off </td>
                        <td> off </td>
                      @endif
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>Facebook Video Post Time</td>
                      <td><input type='text' title="Facebook Video Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeFbVid" id="postTimeFbVid" value='{{$post->postTimeFbVid != '' ? $post->postTimeFbVid : date('1990-07-20 19:00')}}'/></td>
                      @if($post->postToFbVid == '1')
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Tayang</b> </td>
                        @if(empty($post->postApproveFbVid))
                          <td> -- </td>
                        @else
                          <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                        @endif
                      @else
                        <td> off </td>
                        <td> off </td>
                      @endif


                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Instagram TV Post Time</td>
                      <td><input type='text' title="Instagram TV Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeIgTv" id="postTimeIgTv" value='{{$post->postTimeIgTv != '' ? $post->postTimeIgTv : date('1990-07-20 19:00')}}'/></td>
                      @if($post->postToIgTv == '1')
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Tayang</b> </td>
                        @if(empty($post->postApproveIgTv))
                          <td> -- </td>
                        @else
                          <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                        @endif
                      @else
                        <td> off </td>
                        <td> off </td>
                      @endif

                    </tr>
                    <tr>
                      <td>4.</td>
                      <td>Instagram Feed Time</td>
                      <td><input type='text' title="Instagram Feed Time" readonly required class='form-control notfocus datetimepicker' name="postTimeIgFeed" id="postTimeIgFeed" value='{{$post->postTimeIgFeed != '' ? $post->postTimeIgFeed: date('1990-07-20 19:00')}}'/></td>
                      @if($post->postToIgFeed == '1')
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Tayang</b> </td>
                        @if(empty($post->postApproveIgFeed))
                          <td> -- </td>
                        @else
                          <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                        @endif
                      @else
                        <td> off </td>
                        <td> off </td>
                      @endif

                    </tr>
                    <tr>
                      <td>5.</td>
                      <td>All Stories Post Time</td>
                      <td><input type='text' title="All Stories Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeStories" id="postTimeStories" value='{{$post->postTimeStories != '' ? $post->postTimeStories : date('1990-07-20 19:00')}}'/></td>
                      @if($post->postToStories == '1')
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Tayang</b> </td>
                        @if(empty($post->postApproveStories))
                          <td> -- </td>
                        @else
                          <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                        @endif
                      @else
                        <td> off </td>
                        <td> off </td>
                      @endif

                    </tr>
                    <tr>
                      <td>6.</td>
                      <td>Fb Group Post Time</td>
                      <td><input type='text' title="Fb Group Post Time" readonly required class='form-control notfocus datetimepicker' name="postTimeFbGroup" id="postTimeFbGroup" value='{{$post->postTimeFbGroup != '' ? $post->postTimeFbGroup : date('1990-07-20 19:00')}}'/></td>
                      @if($post->postToFbGroup == '1')
                        <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Tayang</b> </td>
                        @if(empty($post->postApproveFbGroup))
                          <td> -- </td>
                        @else
                          <td> <b class="text-success"><i class="fa fa-check-square-o"></i> Approved</b> </td>
                        @endif
                      @else
                        <td> off </td>
                        <td> off </td>
                      @endif

                    </tr>
                  </table>
                </div>
              </div>
            @endif

            <hr>
            <div class="row">
              <div class="col-md-3">
                <label for="postAdsYoutubeType">Youtube Ads Type</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsYoutubeType" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    <option value="Discovery" {{$post->postAdsYoutubeType == "Discovery" ? 'selected' : ''}}>Discovery</option>
                    <option value="Non-skip-5" {{$post->postAdsYoutubeType == "Non-skip-5" ? 'selected' : ''}}>Non-skip-5</option>
                    <option value="Skipable" {{$post->postAdsYoutubeType == "Skipable" ? 'selected' : ''}}>Skipable</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsYoutubeKey">Youtube Ads KPI</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsYoutubeKey" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    <option value="Imppresion" {{$post->postAdsYoutubeKey == "Imppresion" ? 'selected' : ''}}>Imppresion</option>
                    <option value="View" {{$post->postAdsYoutubeKey == "View" ? 'selected' : ''}}>View</option>
                    <option value="Click" {{$post->postAdsYoutubeKey == "Click" ? 'selected' : ''}}>Click</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsYoutubeCost">Youtube Ads Cost</label>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" name="postAdsYoutubeCost" class="form-control" value="{{$post->postAdsYoutubeCost}}" {{$expired == "yes" ? "readonly" : ""}}>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsYoutubeDays">Youtube Ads Days</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsYoutubeDays" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    @for ($i=1; $i < 32; $i++)
                      <option value="{{$i}}" {{$post->postAdsYoutubeDays == $i ? 'selected' : ''}}>{{$i}} days</option>
                    @endfor
                  </select>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-3">
                <label for="postAdsFbType">Facebook Ads Type</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsFbType" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    <option value="Boost Post" {{$post->postAdsFbType == "Boost Post" ? 'selected' : ''}}>Boost Post</option>
                    <option value="CPM" {{$post->postAdsFbType == "CPM" ? 'selected' : ''}}>CPM</option>
                    <option value="CPC" {{$post->postAdsFbType == "CPC" ? 'selected' : ''}}>CPC</option>
                    <option value="CPF" {{$post->postAdsFbType == "CPF" ? 'selected' : ''}}>CPF</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsFbKey">Facebook Ads KPI</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsFbKey" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    <option value="Imppresion" {{$post->postAdsFbKey == "Imppresion" ? 'selected' : ''}}>Imppresion</option>
                    <option value="Reach" {{$post->postAdsFbKey == "Reach" ? 'selected' : ''}}>Reach</option>
                    <option value="Click" {{$post->postAdsFbKey == "Click" ? 'selected' : ''}}>Click</option>
                    <option value="Form" {{$post->postAdsFbKey == "Form" ? 'selected' : ''}}>Form</option>
                    <option value="Follower" {{$post->postAdsFbKey == "Follower" ? 'selected' : ''}}>Follower</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsFbCost">Facebook Ads Cost</label>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" name="postAdsFbCost" class="form-control" value="{{$post->postAdsFbCost}}" {{$expired == "yes" ? "readonly" : ""}}>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsFbDays">Facebook Ads Days</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsFbDays" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    @for ($i=1; $i < 32; $i++)
                      <option value="{{$i}}" {{$post->postAdsFbDays == $i ? 'selected' : ''}}>{{$i}} days</option>
                    @endfor
                  </select>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-3">
                <label for="postAdsIgType">Instagram Ads Type</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsIgType" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    <option value="Reach" {{$post->postAdsIgType == "Reach" ? 'selected' : ''}}>Reach</option>
                    <option value="Click" {{$post->postAdsIgType == "Click" ? 'selected' : ''}}>Click</option>
                    <option value="Form" {{$post->postAdsIgType == "Form" ? 'selected' : ''}}>Form</option>
                    <option value="Follower" {{$post->postAdsIgType == "Follower" ? 'selected' : ''}}>Follower</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsIgKey">Instagram Ads KPI</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsIgKey" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    <option value="Imppresion" {{$post->postAdsIgKey == "Imppresion" ? 'selected' : ''}}>Imppresion</option>
                    <option value="View" {{$post->postAdsIgKey == "View" ? 'selected' : ''}}>View</option>
                    <option value="Click" {{$post->postAdsIgKey == "Click" ? 'selected' : ''}}>Click</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsIgCost">Instagram Ads Cost</label>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" name="postAdsIgCost" class="form-control" value="{{$post->postAdsIgCost}}" {{$expired == "yes" ? "readonly" : ""}}>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <label for="postAdsIgDays">Instagram Ads Days</label>
                <div class="input-group">
                  <select class="form-control" name="postAdsIgDays" {{$expired == "yes" ? "disabled" : ""}}>
                    <option value="">--pilih--</option>
                    @for ($i=1; $i < 32; $i++)
                      <option value="{{$i}}" {{$post->postAdsIgDays == $i ? 'selected' : ''}}>{{$i}} days</option>
                    @endfor
                  </select>
                </div>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <input type="submit" name="submit" value="SUBMIT" class="btn btn-block btn-success" {{$expired == "yes" ? "disabled" : ""}}>
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <button type="button" data-toggle="modal" data-target="#modal-delete" class="pull-left"  {{$expired == "yes" ? "disabled" : ""}}>Delete</button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">

        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header">
              <h4>Approval</h4>
            </div>
            <div class="box-body">
              @if(!empty($post->postStatusProducer))
                <?php
                  $producer = unserialize($post->postStatusProducer);
                  $producer_name = DB::table('cms_users')->where('id',$producer['user'])->first();
                  $producer_time = \Carbon\Carbon::parse($producer['time']);
                 ?>
                <p class="text-success"><i class="fa fa-check-square-o"></i> Approved Producer by : {{$producer_name->name}} <sup> <i class="fa fa-info-circle" data-toggle="tooltip" title="Time : {{$producer_time->diffForHumans()}}, metadata : {{$producer['ipaddress']}}"></i> </sup> </p>
              @else
                @if(CRUDBooster::myPrivilegeID() == '7')
                  <p>Approval : {{CRUDBooster::myName()}}</p>
                  <p>Metadata : {{$agent->device()}} {{$agent->platform()}} {{$agent->version($agent->platform())}} {{$agent->browser()}} {{$agent->version($agent->browser())}}</p>
                  <p>IPAddress : {{Request::ip()}}</p>
                  <a href="{{route('approvalProducer',$post->id)}}" class="btn btn-danger btn-block"  {{$expired == "yes" ? "disabled" : ""}}>Approve</a>
                @else
                  <p class="text-warning"><i class="fa  fa-clock-o"></i> Waiting for Producer Approval</p>
                @endif

              @endif
              <hr>
              @if(!empty($post->postStatusEditor))
                <?php
                  $editor = unserialize($post->postStatusEditor);
                  $editor_name = DB::table('cms_users')->where('id',$editor['user'])->first();
                  $editor_time = \Carbon\Carbon::parse($editor['time']);
                 ?>
                <p class="text-success"><i class="fa fa-check-square-o"></i> Approved Editor by : {{$editor_name->name}} <sup> <i class="fa fa-info-circle" data-toggle="tooltip" title="Time : {{$editor_time->diffForHumans()}}, metadata : {{$editor['ipaddress']}}"></i> </sup> </p>
              @else
                @if(CRUDBooster::myPrivilegeID() == '6')
                  <p>Approval : {{CRUDBooster::myName()}}</p>
                  <p>Metadata : {{$agent->device()}} {{$agent->platform()}} {{$agent->version($agent->platform())}} {{$agent->browser()}} {{$agent->version($agent->browser())}}</p>
                  <p>IPAddress : {{Request::ip()}}</p>
                  <a href="{{route('approvalEditor',$post->id)}}" class="btn btn-danger btn-block"  {{$expired == "yes" ? "disabled" : ""}}>Approve</a>
                @else
                  <p class="text-warning"><i class="fa  fa-clock-o"></i> Waiting for Editor Approval</p>
                @endif

              @endif
              <hr>
              @if(!empty($post->postStatusApproval))
                <?php
                  $client = unserialize($post->postStatusApproval);
                  $client_name = DB::table('cms_users')->where('id',$client['user'])->first();
                  $client_time = \Carbon\Carbon::parse($client['time']);
                 ?>
                <p class="text-success"><i class="fa fa-check-square-o"></i> Approved Client by : {{$client_name->name}} <sup> <i class="fa fa-info-circle" data-toggle="tooltip" title="Time : {{$client_time->diffForHumans()}}, metadata : {{$client['ipaddress']}}"></i> </sup> </p>
              @else
                @if(CRUDBooster::myPrivilegeID() == '5')
                  <p>Approval : {{CRUDBooster::myName()}}</p>
                  <p>Metadata : {{$agent->device()}} {{$agent->platform()}} {{$agent->version($agent->platform())}} {{$agent->browser()}} {{$agent->version($agent->browser())}}</p>
                  <p>IPAddress : {{Request::ip()}}</p>
                  <a href="{{route('approvalClient',$post->id)}}" class="btn btn-danger btn-block" {{$expired == "yes" ? "disabled" : ""}}>Approve</a>
                @else
                  <p class="text-warning"><i class="fa  fa-clock-o"></i> Waiting for Client Approval</p>
                @endif
              @endif
            </div>
            <div class="box-footer">

            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="user-block">
                <h3 class="box-title">Komentar</h3>
              </div>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
              <div class="box-body box-comments" id="internalChatMessages" style="overflow: auto; width:100%; height:250px!important;">
                @foreach($comments as $comment)
                  <?php
                  $user = DB::table('cms_users')->where('id',$comment->user_id)->first();
                  $create = \Carbon\Carbon::parse($comment->created_at);
                  ?>

                  <div class="box-comment">

                    <img class="img-circle img-sm" src="{{url('/')}}/{{ $user->photo }}" alt="User Image">

                    <div class="comment-text">
                      <span class="username">
                        {{ ucfirst($user->name) }}
                        <span class="text-muted pull-right">{{ $create->diffForHumans() }}</span>
                      </span>
                      @if($comment->image!== "tidak")
                        <img src="{{$url}}/uploads/thumbnails/{{$displayAll[$comment->image]}}" alt="" style="margin: 0px 4px 0px 4px;" width="20px" data-toggle="modal" data-target="#modal-image-{{$comment->image}}">
                      @endif
                      {!! $comment->comment !!}
                    </div>
                  </div>
                @endforeach
              </div>
              <div class="box-footer">
                <form action="{{route('postComment',$post->id)}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-xs-8">
                      <img class="img-responsive img-circle img-sm" src="{{ CRUDBooster::myPhoto() }}" alt="Alt Text">
                      <!-- .img-push is used to add margin to elements next to floating images -->
                      <div class="img-push">
                        <input type="text" class="form-control" placeholder="Your comment here" name="comment" row="4" required>
                      </div>
                    </div>
                    <div class="col-xs-4">
                      <div class="form-group">
                        <input type="submit" class="form-control" value="Comment">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>foto display</h4>
            </div>
            <div class="box-body">

              <div class="row">
                @if($displays)
                  @foreach ($displays as $n => $display)
                    <div class="col-md-4" >
                      <p><img src="{{$url}}/uploads/thumbnails/{{$display}}" alt="" width="100%" data-toggle="modal" data-target="#modal-image-{{$n}}"></p>
                      <p><a href="#" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#image-delete-{{$n}}" {{$expired == "yes" ? "disabled" : ""}}>delete</a></p>
                      <hr>
                    </div>
                  @endforeach
                @endif
              </div>

            </div>
            <div class="box-footer">
              <form action="{{route('postAddDisplay',$post->id)}}" method="post" name="addProduct" id="editDisplay" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="Product Name">Product Display (can attach more than one):</label>
                  <br />
                  <input type="file" class="form-control" name="photos[]" multiple {{$expired == "yes" ? "disabled" : ""}}/>
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="submit" name="submitDetail" value="Upload" class="btn btn-success btn-block" {{$expired == "yes" ? "disabled" : ""}}>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Edit VIDEO</h3>
            </div>
            <div class="box-body">
              @if(!empty($post->postVideo))
              <iframe width="100%" height="200" src="https://www.youtube.com/embed/{{$post->postVideo}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              @endif
            </div>
            <div class="box-footer">
              <form action="{{route('postAddVideo',$post->id)}}" method="post" name="changeVideoForm" id="changeVideoForm" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label for="postVideo">URL VIDEO YOUTUBE</label>
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">https://youtube.com/watch?v=</div>
                    <input type="text" class="form-control" name="postVideo" placeholder="ISOGRAM" value="{{$post->postVideo}}" required {{$expired == "yes" ? "disabled" : ""}}/>
                  </div>
                </div>

                <div class="form-group">
                  <input type="submit" name="submit" value="SUBMIT VIDEO" class="btn btn-block btn-success" {{$expired == "yes" ? "disabled" : ""}}>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <a href="{{route('ProjectCalendar')}}" class="btn btn-block btn-primary">BACK to CALENDAR</a>
        </div>
      </div>
  </div>

@if($displays)
@foreach ($displays as $x => $imgmdl)
  <div class="modal fade" id="modal-image-{{$x}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Image Post {{$post->postName}} - {{$x+1}}</h4>
          </div>
          <div class="modal-body">
            <p><img src="{{$url}}/uploads/{{$imgmdl}}" alt="" width="100%"></p>
            @foreach ($imgcmts as $s => $imgc)

              @if($imgc->image == $x)
                <?php
                $user = DB::table('cms_users')->where('id',$imgc->user_id)->first();
                $create = \Carbon\Carbon::parse($imgc->created_at);
                ?>
                <p><img class="img-circle img-sm" src="{{url('/')}}/{{ $user->photo }}" alt="User Image"> &nbsp {{ ucfirst($user->name) }} <sup><i>{{ $create->diffForHumans() }}</i></sup> : {{$imgc->comment}}</p>
              @endif
            @endforeach
          </div>
          <div class="modal-footer">
            <form action="{{route('postComment',$post->id)}}" method="post">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-xs-8">
                  <img class="img-responsive img-circle img-sm" src="{{ CRUDBooster::myPhoto() }}" alt="Alt Text">
                  <div class="img-push">
                    <input type="text" class="form-control" placeholder="Your comment here" name="comment" row="4" required>
                    <input type="hidden" name="image" value="{{$x+1}}">
                  </div>
                </div>
                <div class="col-xs-4">
                  <div class="form-group">
                    <input type="submit" class="form-control" value="Comment">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endforeach

@foreach ($displays as $x => $dlt)
  <div class="modal modal-danger fade" id="image-delete-{{$x}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Danger!!</h4>
        </div>
        <div class="modal-body">
          <p><img src="{{$url}}/uploads/{{$dlt}}" alt="" width="100%"></p>
          <p>Are You Sure want to delete?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <a href="{{route('postImgDelete',[$post->id,$x])}}" type="button" class="btn btn-outline">DELETE</a>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@endforeach
@endif
    <div class="modal modal-danger fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Danger!!</h4>
          </div>
          <div class="modal-body">
            <p>Are You Sure want to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            <a href="{{route('ProjectDeletePostCalendar',$post->id)}}" type="button" class="btn btn-outline">DELETE</a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
